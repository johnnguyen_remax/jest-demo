const testFuncs = require('./functions')

describe("Testing our functions", () => {
    it("correctly multiplies two numbers", () => {
        expect(testFuncs.multiply(3, 5)).toBe(15)
    })
    
})
